﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Car
    {
        private int price;
        private string model;

        public Car(string _model, int _price) { price = _price; model = _model; }
    }
}
